package scalanimate.example2

import org.scalajs.dom

object Main {

  def main(args: Array[String]): Unit = {
    dom.document.writeln("Hello, example2!")
  }

}
