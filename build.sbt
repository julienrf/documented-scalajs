inThisBuild(Seq(
  scalaVersion := "2.12.4"
))

val jsSettings = Seq(
  libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "0.9.2",
  scalaJSUseMainModuleInitializer := true
)

val example1 = project.enablePlugins(ScalaJSPlugin).settings(jsSettings)

val example2 = project.enablePlugins(ScalaJSPlugin).settings(jsSettings)

val index =
  project.in(file("."))
    .settings(
      sourceGenerators in Compile += scalanimate.Website.make(example1, example2)
    )