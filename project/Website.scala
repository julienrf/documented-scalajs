package scalanimate

import org.scalajs.sbtplugin.ScalaJSPlugin.AutoImport.fullOptJS
import sbt.{Def, _}
import sbt.Keys._

object Website {

  def make(example1: Project, example2: Project): Def.Initialize[Task[Seq[File]]] = Def.taskDyn {

    val targetDir = (target in Compile).value / "website"
    val indexFile = targetDir / "index.html"

    Def.task {
      IO.write(indexFile,
        s"""<!doctype html>
           |<html>
           |  <head></head>
           |  <body>
           |    <h1>Scalanimate Tutorial</h1>
           |
           |    <h2>Example 1</h2>
           |    <p>Source code:</p>
           |    <pre><code>${source(example1, "scalanimate/example1/Main.scala", startLine = 8, endLine = 8).value}</code></pre>
           |    <p>Result:</p>
           |    <div>${jsFile(example1, targetDir).value}</div>
           |
           |    <h2>Example 2</h2>
           |    <p>Source code:</p>
           |    <pre><code>${source(example2, "scalanimate/example2/Main.scala", startLine = 7, endLine = 9).value}</code></pre>
           |    <p>Result:</p>
           |    <div>${jsFile(example2, targetDir).value}</div>
           |
           |  </body>
           |</html>
           |""".stripMargin
      )
      Nil
    }

  }

  def source(project: Project, path: String, startLine: Int = 1, endLine: Int = Int.MaxValue): Def.Initialize[Task[String]] = Def.task {
    val source = (scalaSource in Compile in project).value / path
    IO.readLines(source).slice(startLine - 1, endLine).mkString("\n")
  }

  def jsFile(project: Project, targetDir: File) = Def.task {
    val targetFileName = s"${project.id}.js"
    val targetFile = targetDir / targetFileName
    IO.copyFile((fullOptJS in Compile in project).value.data, targetFile)
    s"""<script src="$targetFileName"></script>"""
  }

}
